/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { AppRegistry } from 'react-native';
import { Provider } from 'react-redux';
import configureStore from './src/store/configureStore';
import App from './src/App';

// disable warning in simulator
console.disableYellowBox = true;

class TennisAce extends Component {
    constructor() {
        super();

        this.state = {
            isLoading: true,
            store: configureStore(() => this.setState({ isLoading: false }))
        };
    }

    render() {
        return (
            <Provider store={this.state.store}>
                <App isLoading={this.state.isLoading} />
            </Provider>
        );
    }
}

AppRegistry.registerComponent('TennisAce', () => TennisAce);
