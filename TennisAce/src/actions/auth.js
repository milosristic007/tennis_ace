
import * as types from './actionTypes'

export function setAuthToken(state) {
    return {
        type: types.USER_AUTH_TOKEN,
        authTokenAdmin: state.authTokenAdmin
    }
};

export function setSignUp(state) {
    return {
        type: types.USER_SIGNED_UP,
        authTokenAdmin: state.authTokenAdmin,
        authTokenLogin: state.authTokenLogin,
        username: state.username,
        email: state.email,
        isLoggedIn: true,
    }
};

export function setLogIn(state) {
    return {
        type: types.USER_LOGGED_IN,
        authTokenLogin: state.authTokenLogin,
        username: state.username,
        email: state.email,
        isLoggedIn: true,
    }
};

export function setLogOut() {
    return {
        type: types.USER_LOG_OUT,

    }
};