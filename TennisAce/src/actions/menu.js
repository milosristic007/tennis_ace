import * as types from '../actions/actionTypes'

export function showLeft(state) {
    return {
        type: types.MENU_LEFT_STATE,
        showLeft: state.showLeft
    }
};

export function showRight(state) {
    return {
        type: types.MENU_RIGHT_STATE,
        showRight: state.showRight,
    }
};

export function setNavigator(navigator) {
    return {
        type: types.MENU_NAVIGATOR,
        navigator: navigator,
    }
};
