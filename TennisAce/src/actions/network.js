import * as types from './actionTypes'

export function setIsConnected() {
    return {
        type: types.NET_STATE,
        isConnected
    };
};
