
// for user state
export const USER_AUTH_TOKEN = 'AUTH_TOKEN';
export const USER_LOG_IN = 'LOG_IN';
export const USER_LOGGED_IN = 'LOGGED_IN';
export const USER_SIGN_UP = 'SIGN_UP';
export const USER_SIGNED_UP = 'SIGNED_UP';
export const USER_LOG_OUT = 'LOG_OUT';
export const USER_LOGGED_OUT = 'LOGGED_OUT';

// for network state
export const NET_STATE = 'NET_STATE';

// for menu state
export const MENU_LEFT_STATE = 'MENU_LEFT_STATE';
export const MENU_RIGHT_STATE = 'MENU_RIGHT_STATE';
export const MENU_NAVIGATOR = 'MENU_NAVIGATOR';
