import {
  Alert,
  VibrationIOS,
  Platform,
  PushNotificationIOS,
} from 'react-native';
import { updateInstallation } from './installation';
import { switchTab, openChat } from './navigation';
import { fetchMessageThread } from './messages';

function normalizeData(s) {
  if (s && typeof s === 'object') {
    return s;
  }
  try {
    return JSON.parse(s);
  } catch (e) {
    return {};
  }
}

export async function storeDeviceToken(deviceToken) {
  const pushType = Platform.OS === 'android' ? 'gcm' : undefined;
  await updateInstallation({
    pushType,
    deviceToken,
    deviceTokenLastModified: Date.now(),
  });
  return {
    type: 'REGISTERED_PUSH_NOTIFICATIONS',
  };
}

export function turnOnPushNotifications() {
  return {
    type: 'TURNED_ON_PUSH_NOTIFICATIONS',
  };
}

export function skipPushNotifications() {
  return {
    type: 'SKIPPED_PUSH_NOTIFICATIONS',
  };
}

export function receivePushNotification(notification) {
  return (dispatch) => {
    const { foreground, message } = notification;
    const data = normalizeData(notification.data);
    if (!foreground && data.hasOwnProperty('mid')) {
      dispatch(switchTab('Messages'));
    } else if (!foreground && data.hasOwnProperty('tid')) {
      const threadId = data.tid;
      dispatch(switchTab('Messages'));
      dispatch(openChat(threadId));
    } else if (!foreground && data.hasOwnProperty('pid')) {
      dispatch(switchTab('Feed'));
    }

    if (foreground) {
      if (data.hasOwnProperty('tid')) {
        dispatch(fetchMessageThread(data.tid));
        if (Platform.OS === 'ios') {
          VibrationIOS.vibrate();
        }
      } else {
        Alert.alert(
          'Notification',
          message
        );
        if (Platform.OS === 'ios') {
          VibrationIOS.vibrate();
        }
      }
    }

    const timestamp = new Date().getTime();
    dispatch({
      type: 'RECEIVED_PUSH_NOTIFICATION',
      notification: {
        text: message,
        data: data,
        time: timestamp,
      },
    });
  };
}

export async function markAllNotificationsAsSeen() {
  await updateInstallation({
    badge: 0
  });
  if (Platform.OS === 'ios') {
    PushNotificationIOS.setApplicationIconBadgeNumber(0);
  }
  return {
    type: 'SEEN_ALL_NOTIFICATIONS',
  };
}
