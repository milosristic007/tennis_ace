/* @flow */

import { createStore, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';
import thunkMiddleware from 'redux-thunk';
import { persistStore, autoRehydrate } from 'redux-persist';
import { AsyncStorage } from 'react-native';

import { promise } from './promise';
import reducers from '../reducers';


const middlewares = [thunkMiddleware, promise];

if (__DEV__) {
  const logger = createLogger({ collapsed: true });
  middlewares.push(logger);
}

const createStoreWithMiddleware = applyMiddleware(...middlewares)(createStore);

function configureStore(onComplete: Function) {
  const store = autoRehydrate()(createStoreWithMiddleware)(reducers);
  persistStore(
    store,
    {
      whitelist: ['user', 'network'],
      storage: AsyncStorage
    },
    onComplete
  );

  return store;
}

export default configureStore;
