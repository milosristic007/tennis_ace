import React from 'react';
import { Row, View, Image, Subtitle, Text, Caption, Icon, Divider, TouchableOpacity, StyleSheet } from '@shoutem/ui';
import moment from 'moment';

class ListNotificationItem extends React.Component {

    render() {
        const { constents } = this.props.data;

        return (
            <TouchableOpacity onPress={() => this.props.onPressItem(this.props.data)}>
                <Row>
                    <View styleName="vertical stretch space-between">
                        <Subtitle styleName="top" style={styles.tipText}>{this.props.data.title}</Subtitle>
                        <Caption>Buy now</Caption>
                    </View>
                    <Icon styleName="disclosure" name="right-arrow" />
                </Row>
                <Divider styleName="line" />
            </TouchableOpacity>
        );
    }
}

const styles = {
    tipType: {
        padding: 3, 
        paddingLeft: 10, 
        paddingRight: 10,
        // alignItems: 'center',
        justifyContent: 'center'
    },
    tipText: {
        color: 'black',
    }
}

export default ListNotificationItem;
