import React from 'react';
import { Row, View, Image, Subtitle, Text, Caption, Icon, Divider, TouchableOpacity } from '@shoutem/ui';
import moment from 'moment';

class ListBuyItem extends React.Component {

    render() {
        const { item } = this.props.data;

        return (
            <TouchableOpacity onPress={this.props.onPressItem}>
                <Row>
                    <View styleName="horizontal stretch space-between" style={styles.container}>
                        <View styleName="horizontal stretch space-between" style={styles.container}>
                            <Image
                                styleName="small rounded-corners"
                                source={{ uri: this.props.data.bookie_logo }}
                                style={{ backgroundColor: '#f1f1f1', width:75, height:50, marginRight:10, resizeMode:'cover' }}
                            />
                            <Subtitle style={styles.textColor}>{this.props.data.odds}</Subtitle>
                        </View>
                        <Caption style={styles.textColor}>{this.props.data.bookie_slug}</Caption>
                    </View>
                    <Icon styleName="disclosure" name="right-arrow" />
                </Row>
                <Divider styleName="line" />
            </TouchableOpacity>
        );
    }
}

const styles = {
    container: {
        alignItems: 'center',
    },
    textColor: {
        color: 'gray',
    }
}

export default ListBuyItem;
