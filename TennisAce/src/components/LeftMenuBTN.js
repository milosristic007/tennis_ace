import React, { Component } from 'react';
import { Image, TouchableOpacity } from 'react-native';

export default class LeftMenuBTN extends Component {
    render() {
        return (
            <TouchableOpacity onPress={this.props.onPress} style={this.props.style}>
                <Image
                    source={require('../assets/menu_left.png')}
                    style={{ height: 20, width: 20 }}
                />
            </TouchableOpacity>
        );
    }
}