import React from 'react';
import { Row, View, Image, Subtitle, Text, Caption, Icon, Divider, TouchableOpacity, StyleSheet } from '@shoutem/ui';
import moment from 'moment';

class ListItem extends React.Component {

    render() {
        const { constents } = this.props.data;

        return (
            <TouchableOpacity onPress={() => this.props.onPressItem(this.props.data)}>
                <Row>
                    <View styleName="vertical stretch space-between">
                        <View styleName="horizontal stretch space-between">
                            <Caption>{this.props.data.sport_type}</Caption>
                            <Caption>{moment(this.props.data.event_date).format('hh:mm A')}</Caption>
                        </View>
                        <Subtitle styleName="top">{this.props.data.title}</Subtitle>
                        <View styleName="horizontal stretch space-between">
                            {this.props.data.product_price != null && this.props.data.product_price != '' ?
                                <View style={styles.tipType}>
                                    <Text style={styles.tipText}>GOLDEN{/*this.props.data.tip_type.toUpperCase()*/}</Text>
                                </View>
                                :
                                <Caption>{this.props.data.tip_type}</Caption>
                            }
                            <Caption>{this.props.data.Confidence} confidence</Caption>
                            <Caption>{this.props.data.bookies[0].odds}</Caption>
                        </View>
                    </View>
                    <Icon styleName="disclosure" name="right-arrow" />
                </Row>
                <Divider styleName="line" />
            </TouchableOpacity>
        );
    }
}

const styles = {
    tipType: {
        backgroundColor: '#d8c80c',
        borderRadius: 12,
        alignItems: 'center',
        justifyContent: 'center'
    },
    tipText: {
        padding: 3, 
        paddingLeft: 10, 
        paddingRight: 10,
        color: 'white',
    }
}

export default ListItem;
