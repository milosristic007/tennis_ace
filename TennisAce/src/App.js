import React, { Component, PropTypes } from 'react';
import { ActivityIndicator, AppState, Text, View, StatusBar, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import {Router, Scene, Actions, ActionConst} from 'react-native-router-flux';

import Login from './containers/Login';
import Register from './containers/Register';
import Home from './containers/Home';
import ItemDetailView from './containers/ItemDetailView';
import AppNavigator from './containers/AppNavigator';
import NavigationDrawer from './containers/NavigationDrawer';



class App extends Component {

  renderNoConnctionBanner() {
    return (
      <View
        style={{
          height: 30,
          backgroundColor: 'red',
          justifyContent: 'center',
          alignItems: 'center'
        }}
      >
        <Text style={{ fontSize: 12, color: '#fff' }}>No Internet Connection</Text>
      </View>
    );
  }

  render() {
    const { isLoggedIn, isConnected, isLoading } = this.props;

    return (
      <Router>
        <Scene key="root">
          {!isConnected ? this.renderNoConnctionBanner() : null}
          {/*{isLoggedIn ? */}
          <Scene key="Register" component={Register} hideNavBar />
          <Scene key="Login" component={Login} hideNavBar/>
          <Scene key="NavigationDrawer" component={NavigationDrawer} open={false} type={ActionConst.REPLACE} >
            <Scene key="AppNavigator" component={AppNavigator} >
              <Scene key="Home" component={Home} initial />
                <Scene key="ItemDetailView" component={ItemDetailView} />
            </Scene>
          </Scene>
        </Scene>
      </Router>
    );

  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  indicator: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
	navBar: {
		flex: 1,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: 'green',
	},
	navTitle: {
		color: 'white',
	},
	leftButtonContainer: {
		paddingLeft: 15,
		paddingRight: 20,
		flex: 1,
		flexDirection: 'row',
		alignItems: 'center',
	},
});

App.propTypes = {
  isLoggedIn: PropTypes.bool.isRequired,
  isConnected: PropTypes.bool.isRequired,
  isLoading: PropTypes.bool.isRequired,
  dispatch: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired
};

function select(store) {
  return {
    isLoggedIn: store.user.isLoggedIn,
    isConnected: store.network.isConnected,
    user: store.user
  };
}


export default connect(select)(App);
