'use-strict'

export const API_URL_AUTH = 'https://www.betset.com.au/wp-json';
export const API_URL_SINUP = API_URL_AUTH + '/wp/v2/users';
export const API_URL_LOGIN = API_URL_AUTH + '/jwt-auth/v1/token';
export const API_URL_LOGIN_TOKEN = API_URL_AUTH + '/jwt-auth/v1/token/validate';
export const API_URL_CONTENTS = API_URL_AUTH + '/betset/v1/tennis';
export const API_URL_PURCHASE = API_URL_AUTH + '/betset/v1/users/meta';

export const STORAGE_KEY = 'authToken';
export const STORAGE_LOGIN_TOKEN = 'loginToken';
export const STORAGE_CONTENTS = 'contents';

export const ONE_SIGNAL_TEST_ID = '73a522ac-dc37-45f1-9d67-2791f3247179';
export const ONE_SIGNAL_APP_ID = '51b85ea5-92df-46b0-bd54-5bf7dd4c0810';
export const ONE_SIGNAL_REST_API_KEY = 'OTdhZjE3YzMtMDdjNy00ZWFhLWJjY2QtYWU1OWNmNTRhOWY1';

export const ADMOB_ID_IOS = 'ca-app-pub-3839351587918716/1270854586';
export const ADMOB_ID_ANDROID = 'ca-app-pub-3839351587918716/1131253786';
export const ADMOB_ID_TESTER = 'ca-app-pub-3940256099942544/1033173712';

export const IAP_ID_TIP_PRODUCT = 'com.betset.tennisace.';
export const IAP_ID_TIP_SERVICE = 'com.betset.tennisace.tipservice';
export const IAP_ID_TIP_SERVICE01 = 'com.betset.tennisace.tipservice01';
export const IAP_ID_TIP_SERVICE02 = 'com.betset.tennisace.tipservice02';
export const IAP_ID_TIP_SERVICE03 = 'com.betset.tennisace.tipservice03';
export const IAP_ID_TIP_SERVICE04 = 'com.betset.tennisace.tipservice04';
export const IAP_ID_TIP_SERVICE05 = 'com.betset.tennisace.tipservice05';
