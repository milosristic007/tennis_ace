import React, { Component, PropTypes } from 'react'
import { StyleSheet } from 'react-native'
import { NavigationBar, View, ListView, Row, Button, Title, Subtitle, Text, Caption, Icon, Divider } from '@shoutem/ui';
import moment from 'moment';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux'

import ListBuyItem from '../components/ListBuyItem'
import * as authActions from '../actions/auth'

class TipPayment extends Component {
	constructor(props) {
		super(props);

        this.state = {
            items: [{
                imgURL: '',
                price: '$4.5',
            },
            {
                imgURL: '',
                price: '$4.5',
            },
            ]
        };
	}

    onPressItem = (item) => {
    }

	render() {
		// const { drawer } = this.context
		return (
            <View style={styles.container}>
                <NavigationBar
                hasHistory
                styleName="no-border"
                leftComponent={
                    <Button styleName="clear" onPress={this.props.navigator.pop}>
                        <Icon name="left-arrow" style={{color:'#387afe'}} />
                        <Text style={{color:'#387afe', fontSize: 18}}>Tips</Text>
                    </Button> }
                style={{
                    container: {
                        paddingLeft: 0,
                        paddingBottom: 6
                    },
                    leftComponent: {
                        paddingLeft: 0,
                        marginLeft: 0,
                    }
                }}
                />
                <View style={styles.contentView}>
                    <View style={{ alignItems: 'center' }}>
                        <View styleName="horizontal space-between" style={styles.badgeType}>
                            <Text style={styles.badgeText}>{this.props.data.tip_type.toUpperCase()}</Text>
                        </View>
                    </View>
                    <View style={styles.titleView}>
                        <Text style={styles.title}>{this.props.data.title}</Text>
                    </View>
                    <View styleName="horizontal stretch space-between">
                        <Caption style={styles.tipType}>{this.props.data.sport_type}</Caption>
                        <Caption style={styles.tipType}>{moment(this.props.data.event_date).format('hh:mm A')}</Caption>
                    </View>
                    <View styleName="vertical" style={ styles.tipGoldView }>
                        <Caption style={styles.tipViewText}>Our Tip</Caption>
                        <Subtitle styleName="top" style={styles.subTitle}>$1.99</Subtitle>
                        <TouchableOpacity style={styles.button} onPress={this.requestLogIn} activeOpacity={.5}>
                            <Text style={styles.buttonText}>Pay now</Text>
                        </TouchableOpacity>
                        <Text style={styles.tipType} >
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incidicunt ut labore et dolore magna wirl aliqua.
                        </Text>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = {
    container: {
        flex: 1,
    },
    contentView: {
        marginTop: 70,
        // padding: 10,
    },
    titleView: {
        marginTop: 10,
        marginBottom: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },
    title: {
        paddingLeft: 30,
        paddingRight: 30,
        fontSize: 18,
        fontWeight: '500',
        textAlign: 'center'
    },
    badgeType: {
        // flex: 1,
        width: 80,
        backgroundColor: '#d8c80c',
        borderRadius: 12,
        alignItems: 'center',
        justifyContent: 'center'
    },
    badgeText: {
        padding: 3,
        paddingLeft: 10,
        paddingRight: 10,
        color: 'white',
        textAlign: 'center',
    },
    tipType: {
        color: 'gray',
        padding: 15,
    },
    tipGoldView: {
        padding: 10,
        backgroundColor: '#d8c80c'
    },
    tipView: {
        padding: 10,
        backgroundColor: '#387afe'
    },
    tipViewText: {
        marginTop: 5,
        marginBottom: 5,
        paddingLeft: 5,
        paddingRight: 5,
        color: 'white',
    },
    subTitle: {
        marginTop: 5,
        marginBottom: 5,
        paddingLeft: 5,
        paddingRight: 5,
        fontSize: 18,
        fontWeight: '500',
        color: 'white',
    },
    button: {
        flexDirection: 'row',
        marginVertical: 10,
        marginTop: 30,
        height: 50,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#387afe",
    },
    buttonText: {
        flex: 1,
        color: "#FFF",
        fontSize: 18,
        fontWeight: '500',
        textAlign: 'center'
    },
    purchase: {
        margin: 0,
        paddingTop: 0,
        paddingBottom: 0,
    }
}

TipPayment.propTypes = {
}

function select(store) {
	return {
		authTokenLogin: store.user.authTokenLogin,
	};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(authActions , dispatch);
}

export default connect(select, mapDispatchToProps)(TipPayment)