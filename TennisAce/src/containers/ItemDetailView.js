import React, { Component, PropTypes } from 'react'
import { Linking, ActivityIndicator, ScrollView, TouchableOpacity, AlertIOS, Platform } from 'react-native'
import { NavigationBar, View, ListView, StyleSheet, Row, Button, Title, Subtitle, Text, Caption, Icon, Divider } from '@shoutem/ui'
import moment from 'moment'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Actions } from 'react-native-router-flux'
import { NativeModules } from 'react-native'
import HTMLView from 'react-native-htmlview'

import ListBuyItem from '../components/ListBuyItem'
import * as authActions from '../actions/auth'
import * as appConfig from '../data/appconfig'

const { InAppUtils } = NativeModules;
const InAppBilling = require("react-native-billing");


class ItemDetailView extends Component {
	constructor(props) {
		super(props);

        this.state = {
            visible: false,
            purchase: this.props.data.user_has_purchased,
            items: [{
                imgURL: '',
                price: '$4.5',
            },
            {
                imgURL: '',
                price: '$4.5',
            },
            ]
        };

        console.log('purchase state: ', this.state.purchase);
	}

    componentWillMount() {
        var products = [appConfig.IAP_ID_TIP_SERVICE,appConfig.IAP_ID_TIP_SERVICE01,appConfig.IAP_ID_TIP_SERVICE02,
        appConfig.IAP_ID_TIP_SERVICE03,appConfig.IAP_ID_TIP_SERVICE04,appConfig.IAP_ID_TIP_SERVICE05,];
        if (Platform.OS == 'ios') {

            if (this.props.data.product_price != null && this.props.data.product_price != '') {
                InAppUtils.restorePurchases((error, response) => {
                    if (error) {
                        AlertIOS.alert('itunes Error', 'Could not connect to itunes store.');
                    } else {
                        if (response.length == 0) {
                            // AlertIOS.alert('No Purchases', "We didn't find any purchases to restore.");
                            console.log('We didn\'t find any purchases to restore.');
                            return;
                        }

                        response.forEach(function (purchase) {
                            if (purchase.productIdentifier == this.props.data.product_id_app_store) {
                                // Handle purchased product.
                                this.setState({ purchase: true });
                            }
                        });
                    }
                });
                console.log('IAP products: ' + products);
            }
        }
    }

    onPressItem = (item) => {
        Linking.canOpenURL(item.affiliate_url).then(supported => {
            if (supported) {
                Linking.openURL(item.affiliate_url);
            } else {
                console.log('Don\'t know how to open URI: ' + this.props.url);
            }
        });
    }

    postPurchase = async () => {
        try {
            console.log('3. post purchase item to the server');
			let data = {
                "betset_tips_purchased_ids": this.props.data.id,
			}
			let json = JSON.stringify(data);
            let response = await fetch(appConfig.API_URL_PURCHASE, {
                method: 'POST',
                headers: {
                    'Authorization': 'Bearer ' + this.props.authTokenLogin.token,
                    // 'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: json,
            });
            console.log('meta response: ', JSON.stringify(response));
            if (response.ok) {
                let jsonData = await response.json();
                console.log('meta detail response: ', jsonData);
                return jsonData;
            }
        } catch (e) {
            // this.setState({ isLoading: false, error: true });
            console.log('3 -- error:', e);
        }
    }

    async onPurchase() {
        this.setState({visible: true});
        var products = [appConfig.IAP_ID_TIP_SERVICE,appConfig.IAP_ID_TIP_SERVICE01,appConfig.IAP_ID_TIP_SERVICE02,appConfig.IAP_ID_TIP_SERVICE03,
        appConfig.IAP_ID_TIP_SERVICE04,appConfig.IAP_ID_TIP_SERVICE05,];
        if (Platform.OS == 'ios') {
            InAppUtils.loadProducts(products, (error, products) => {
                console.log('porducts: ', products);
                console.log('Purchase product id: ', this.props.data.product_id_app_store);
                if (error !== null) {
                    AlertIOS.alert('products error', JSON.stringify(error));
                }
                // alert('Purchase product id: ' + products);
                InAppUtils.purchaseProduct(this.props.data.product_id_app_store, (error, response) => {
                    // NOTE for v3.0: User can cancel the payment which will be availble as error object here.
                    if (response && response.productIdentifier) {
                        // AlertIOS.alert('Purchase Successful', 'Your Transaction ID is ' + response.transactionIdentifier);
                        //unlock store here.
                        try {
                            let retValue = this.postPurchase();
                            this.setState({ purchase: true });
                        } catch (error) {
                            console.log('purchase post error', error);
                        }
                    }
                    else if (error !== null) {
                        AlertIOS.alert('products purchase', JSON.stringify(error));
                        console.log('purchase error: ', error);
                    }
                    console.log('purchase response: ', response);
                    this.setState({ visible: false });
                });
            });
        }
        else {
            await InAppBilling.close();
            try {
                InAppBilling.open()
                    .then(() => InAppBilling.purchase(this.props.data.product_id_google_play))
                    .then((details) => {
                        console.log("You purchased: ", details);
                        InAppBilling.consumePurchase(this.props.data.product_id_google_play)
                            .then((details) => {
                                let retValue = this.postPurchase();
                                this.setState({ purchase: true });
                                this.setState({ visible: false });
                                return InAppBilling.close()
                            })
                            .catch((err) => {
                                console.log(err);
                                this.setState({ visible: false });
                                return InAppBilling.close()
                            });
                    })
                    .catch((err) => {
                        console.log(err);
                        this.setState({ visible: false });
                    });
            } catch (err) {
                console.log(err);
                this.setState({ visible: false });
                alert(err);
            }
        }
    }

    renderPurchase = () => {
        return (
           <View styleName="vertical stretch" style={styles.purchaseView}>
                <Caption style={styles.tipViewText}>Our Tip</Caption>
                <Subtitle styleName="top" style={styles.subTitle}>{this.props.data.product_price}</Subtitle>
                <TouchableOpacity style={styles.button} onPress={() => { this.onPurchase() }} activeOpacity={.5}>
                    <Text style={styles.buttonText}>Pay now</Text>
                </TouchableOpacity>
                <Text style={styles.tipViewText} >
                    Golden Tips are paid tips which our team of experts have high confidence in, or have the potential for high returns
                </Text>
            </View>
        );
    }

	render() {
		return (
            <View style={styles.container}>
                <NavigationBar
                    hasHistory
                    styleName="no-border"
                    leftComponent={
                        <Button styleName="clear" onPress={this.props.navigator.pop}>
                            <Icon name="left-arrow" style={{ color: '#387afe' }} />
                            <Text style={{ color: '#387afe', fontSize: 18 }}>Tips</Text>
                        </Button>}
                    style={{
                        container: {
                            paddingLeft: 0,
                            paddingBottom: 6
                        },
                        leftComponent: {
                            paddingLeft: 0,
                            marginLeft: 0,
                        }
                    }}
                />
                <View style={styles.contentView}>
                    <ActivityIndicator
                        animating={this.state.visible}
                        size="large"
                        style={styles.indicator}
                    />
                    {this.props.data.product_price != null && this.props.data.product_price != '' ?
                        <View style={{alignItems: 'center'}}>
                            <View styleName="horizontal space-between" style={styles.badgeType}>
                                <Text style={styles.badgeText}>GOLDEN</Text>
                            </View>
                        </View>
                        :
                        null
                    }
                    <View style={styles.titleView}>
                        <Title styleName="bold" style={styles.title}>{this.props.data.title}</Title>
                    </View>
                    <View styleName="horizontal stretch space-between">
                        <Caption style={styles.tipType}>{this.props.data.sport_type}</Caption>
                        <Caption style={styles.tipType}>{moment(this.props.data.event_date).format('hh:mm A')}</Caption>
                    </View>
                    {(this.props.data.product_price != null && this.props.data.product_price != '' && (this.state.purchase == null || this.state.purchase == false)) ?
                        this.renderPurchase()
                        :
                        <View style={styles.detailView}>
                            <View styleName="vertical stretch space-between" style={this.props.data.tip_type == 'Golden' ? styles.tipGoldView : styles.tipView}>
                                <Caption style={styles.tipViewText}>Our Tip</Caption>
                                <Subtitle styleName="top" style={styles.subTitle}>{this.props.data.our_tip}</Subtitle>
                                <Caption style={styles.tipViewText}>{this.props.data.Confidence} confidence</Caption>
                            </View>
                            <ScrollView style={styles.scrollView}>
                                <View style={styles.detailContentView}>
                                    <HTMLView stylesheet={styles.tipType} value={this.props.data.content} />
                                </View>
                                <View style={styles.purchaseListView}>
                                    {this.props.data.bookies === null ?
                                        null
                                        :
                                        <ListView
                                            data={this.props.data.bookies}
                                            loading={false}
                                            renderRow={(item) => <ListBuyItem data={item} type='View2' style={styles.purchase} onPressItem={() => {this.onPressItem(item)}} />}
                                        />
                                    }
                                </View>
                            </ScrollView>
                        </View>
                    }
                </View>
            </View>
        )
    }
}

const styles = {
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    contentView: {
        flex: 1,
        marginTop: 70,
        // padding: 10,
    },
    scrollView: {
        flex: 1,
    },
    badgeType: {
        // flex: 1,
        width: 80,
        borderRadius: 12,
        backgroundColor: '#d8c80c',
        alignItems: 'center',
        justifyContent: 'center'
    },
    badgeText: {
        padding: 3,
        paddingHorizontal: 10,
        color: 'white',
        textAlign: 'center',
    },
    titleView: {
        marginVertical: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },
    title: {
        paddingHorizontal: 30,
        // fontSize: 18,
        // fontWeight: '500',
        textAlign: 'center'
    },
    tipType: {
        color: 'gray',
        paddingVertical: 15,
        paddingHorizontal: 20,
    },
    tipGoldView: {
        padding: 10,
        backgroundColor: '#d8c80c'
    },
    tipView: {
        padding: 10,
        backgroundColor: '#387afe'
    },
    tipViewText: {
        marginVertical: 5,
        paddingHorizontal: 10,
        color: 'white',
    },
    subTitle: {
        marginVertical: 5,
        paddingHorizontal: 10,
        fontSize: 18,
        fontWeight: '500',
        color: 'white',
    },
    purchaseListView: {
        margin: 0,
        paddingVertical: 0,
    },
    purchaseView: {
        flex: 1,
        padding: 10,
        backgroundColor: '#d8c80c'
    },
    detailView: {
        flex: 1,
        backgroundColor: 'white'
    },
    detailContentView: {
        flex: 1,
        backgroundColor: 'white',
        paddingVertical: 15,
        paddingHorizontal: 20,
    },
    button: {
        flexDirection: 'row',
        marginVertical: 25,
        marginHorizontal: 10,
        height: 50,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "white",
    },
    buttonText: {
        flex: 1,
        color: 'gray',
        fontSize: 18,
        fontWeight: '500',
        textAlign: 'center'
    },
}

ItemDetailView.propTypes = {
}

function select(store) {
	return {
		authTokenLogin: store.user.authTokenLogin,
	};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(authActions , dispatch);
}

export default connect(select, mapDispatchToProps)(ItemDetailView)