import React, { Component, PropTypes } from 'react';
import {
  ActivityIndicator,
  Dimensions,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  AsyncStorage,
  AlertIOS,
  StyleSheet
} from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';

import * as Configs from '../data/appconfig';
import * as authActions from '../actions/auth';

async function timeout(ms) {
  return new Promise((resolve, reject) => {
    setTimeout(() => reject(new Error('Timed out')), ms);
  });
}

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      authTokenLogin: null,
      username: '',//zoltan_teszari',
      password: '',//123456789',
      isLoading: false,
    };
    this.requestLogIn = this.requestLogIn.bind(this);
    this.onLogIn = this.onLogIn.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
   }

  componentWillUnmount() {
    this._isMounted = false;
  }

	async getAuthToken(username, password){
		try{
			let data = {
				username: username,
				password: password
			}
			let json = JSON.stringify(data);
			let response = await fetch(Configs.API_URL_LOGIN, {
				method: 'POST',
				headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json',
				},
				body: json
			});
			if (response.ok) {
				let responseJson = await response.json();
				if (responseJson.token){
					return responseJson;
				}
        return null;
			} else {
        return null;
				// throw new Error('Network response was not ok.');
			}
		} catch (error) {
			console.log('Fetch error', error);
		}
	}

  async requestLogIn() {
		const wpAppUserUsername = this.state.username;
		const wpAppUserPassword = this.state.password;
    if( wpAppUserUsername === '' || wpAppUserPassword ==='' ) {
      alert('please put the correct name and password!')
    }
    else {
      try{
        let responseJson = await this.getAuthToken(wpAppUserUsername, wpAppUserPassword);
        console.log('Response from server', responseJson);
        if (responseJson !== null) {
          this.setState({ authTokenLogin: responseJson });
          AsyncStorage.setItem(Configs.STORAGE_LOGIN_TOKEN, JSON.stringify(responseJson));
          this.props.setLogIn(this.state);
          Actions.NavigationDrawer();
        }
        else {
          alert('please put the correct name and password!')
        }
      } catch (error) {
        console.log('Set admin token error', error);
      }
		}
  }

  async onLogIn() {
    this.setState({ isLoading: true });
    try {
      await Promise.race([
        this.requestLogIn(),
        timeout(15000),
      ]);
      this.setState({ isLoading: false });
    } catch (e) {
      const message = e.message || e;
      if (message !== 'Timed out' && message !== 'Canceled by user') {
        alert(message);
      }
      this.setState({ isLoading: false });
      return;
    } finally {
      if (this._isMounted) {
        this.setState({ isLoading: false });
      }
    }
  }

  render() {
    return (
      <View style={styles.container}>
            <View style={styles.container}>
              <View style={styles.wrapper}>
                <View style={styles.inputWrap}>
                  <TextInput 
                    placeholder="Username" 
                    placeholderTextColor="#000"
                    underlineColorAndroid='transparent'
                    style={styles.input} 
                    onChange={ (event) => this.setState({username: event.nativeEvent.text}) } 
                    value={ this.state.username }
                  />
                </View>
                <View style={styles.inputWrap}>
                  <TextInput 
                    placeholder="Password" 
                    placeholderTextColor="#000"
                    underlineColorAndroid='transparent'
                    style={styles.input} 
                    secureTextEntry 
                    onChange={ (event) => this.setState({password: event.nativeEvent.text}) } 
                    value={ this.state.password }
                  />
                </View>
                <TouchableOpacity style={styles.button} onPress={this.requestLogIn} activeOpacity={.5}>
                  <Text style={styles.buttonText}>Sign in now</Text>
                </TouchableOpacity>
              </View>
              <View style={styles.container}>
                <View style={styles.signupWrap}>
                  <Text style={styles.accountText}>
                    Don't have a BetSet account?
                  </Text>
                </View>
                <TouchableOpacity  onPress={Actions.Register} activeOpacity={.5}>
                  <View>
                    <Text style={styles.signupLinkText}>Register now</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  wrapper: {
    paddingHorizontal: (Dimensions.get('window').width<641?30:(Dimensions.get('window').width-500)/2),
    backgroundColor: 'transparent', 
    marginTop: (Dimensions.get('window').height<800 ? 80 : 200), 
    alignItems: 'center'
  },
  inputWrap: {
    flexDirection: "row",
    marginVertical: 10,
    height: 40,
    borderBottomWidth: 1,
    borderBottomColor: "#CCC"
  },
  input: {
    flex: 1,
    paddingHorizontal: 10
  },
  button: {
    flexDirection: 'row',
    marginVertical: 10,
    marginTop: 30,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#387afe",
  },
  buttonText: {
    flex: 1,
    color: "#FFF",
    fontSize: 18,
    fontWeight:'500',
    textAlign: 'center'
  },
  signupWrap: {
    marginTop: 35,
    backgroundColor: 'transparent',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  accountText: {
    color: 'gray'
  },
  signupLinkText: {
    color: '#387afe',
    fontSize: 18,
    fontWeight: '500',
    textAlign: 'center'
  }
});

Login.propTypes = {
  authTokenLogin: PropTypes.object,
  isLoading: PropTypes.bool
};

function select(store) {
  return {
    isLoading: store.user.isLoading,
    authTokenLogin: store.user.authTokenLogin,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(authActions , dispatch);
}

export default connect(select, mapDispatchToProps)(Login)