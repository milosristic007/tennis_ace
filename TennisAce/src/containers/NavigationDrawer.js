import React, { Component, PropTypes } from 'react';
import Drawer from 'react-native-drawer';
import {Actions, DefaultRenderer} from 'react-native-router-flux';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import SideMenu from './SideMenu';
import RightMenu from './RightMenu';

const drawerStyles = {
  drawer: { shadowColor: '#000000', shadowOpacity: 0.8, shadowRadius: 3},
  main: {paddingLeft: 3},
};

export default class NavigationDrawer extends Component {

    constructor(props) {
        super(props);

        this.state = {
            showLeft: false,
            showRight: false,
        };
    }

    render(){
        const state = this.props.navigationState;
        const children = state.children;

        return (
            <Drawer
                ref="navigation"
                open={state.showLeft}
                type="overlay"
                side="left"
                style={drawerStyles}
                content={<SideMenu />}
                tapToClose={true}
                openDrawerOffset={0.2}
                panCloseMask={0.2}
                negotiatePan={true}
                tweenHandler={(ratio) => ({
                 main: { opacity: Math.max(0.54,1-ratio) },
                 mainOverlay: {opacity: Math.min(0.8, ratio), backgroundColor: '#000000'}
                })}>
                {/*<Drawer
                    ref="navigation"
	            	open={state.showRight}
                    type="overlay"
                    side="right"
                    style={drawerStyles}
                    content={<RightMenu navigator={state}/>}
                    tapToClose={true}
                    openDrawerOffset={0.2}
                    panCloseMask={0.2}
                    negotiatePan={true}
                    tweenHandler={(ratio) => ({
                        main: { opacity: Math.max(0.54, 1 - ratio) },
                        mainOverlay: { opacity: Math.min(0.8, ratio), backgroundColor: '#000000' }
                    })}>*/}
                    <DefaultRenderer navigationState={children[0]} onNavigate={this.props.onNavigate}/>
                {/*</Drawer>*/}
            </Drawer>
        );
    }
}


NavigationDrawer.propTypes = {
}