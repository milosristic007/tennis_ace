import React, { Component, PropTypes } from 'react'
import { View, Text, TouchableOpacity, StyleSheet, AsyncStorage } from 'react-native'
import Button from 'react-native-button'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Actions, ActionConst } from 'react-native-router-flux'

import * as Configs from '../data/appconfig'
import * as authActions from '../actions/auth'

class SideMenu extends Component {
	constructor(props) {
		super(props);

		this.state = {
			authTokenLogin: null,
			firstname: '',
			lastname: ''
		};
	}

	static contextTypes = {
		drawer: PropTypes.object.isRequired,
	};

	async logout() {
		AsyncStorage.removeItem(Configs.STORAGE_LOGIN_TOKEN);
		this._closeLeftDrawer();
		this.props.setLogOut();
		Actions.Login({ type: ActionConst.RESET });
	}

	_closeLeftDrawer = () => Actions.refresh({ key: 'NavigationDrawer', showLeft: false, showRight: false })

	render() {
		const { drawer } = this.context
		return (
			<View style={styles.container} >
				<View style={styles.nameContainer}>
					<TouchableOpacity onPress={() => { this._closeLeftDrawer() }} activeOpacity={.5}>
						<View>
							<Text style={styles.nameStyle}>{this.props.authTokenLogin !== null ? this.props.authTokenLogin.user_display_name : ''}</Text>
						</View>
					</TouchableOpacity>
				</View>
				<View style={styles.itemsContainer}>
					<Button style={styles.logoutStyle} onPress={() => { this.logout(); }}>{'LogOut'}</Button>
				</View>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff'
	},
	nameContainer: {
		height: 150,
		justifyContent: 'flex-end',
	},
	nameStyle: {
		fontSize: 20,
    	fontWeight:'500',
		marginLeft: 20,
		marginBottom: 40,
	},
	itemsContainer: {
		flex: 1,
		justifyContent: 'flex-end',
		alignItems: 'flex-start',
		backgroundColor: '#f7f7f7',
	},
	logoutStyle: {
		marginLeft: 20,
		marginBottom: 20,
	}
});

SideMenu.propTypes = {
	// drawer: PropTypes.object,
	// authTokenLogin: PropTypes.object.isRequired,
}

function select(store) {
	return {
		authTokenLogin: store.user.authTokenLogin,
	};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(authActions , dispatch);
}

export default connect(select, mapDispatchToProps)(SideMenu)