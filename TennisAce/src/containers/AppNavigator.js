import React, { Component, PropTypes } from 'react';
import { Navigator, BackAndroid } from 'react-native';

import Home from './Home';

var _navigator;

BackAndroid.addEventListener('hardwareBackPress', () => {
  if (_navigator.getCurrentRoutes().length === 1) {
    return false;
  }
  _navigator.pop();
  return true;
});


export default class AppNavigator extends Component {
  constructor(props) {
    super(props);
  }  
  renderScene(route, navigator) {
    _navigator = navigator
    let RouteComponent = route.component
    return <RouteComponent navigator={navigator} {...route.passProps} />
  }

  componentWillUnmount() {
    BackAndroid.removeEventListener('hardwareBackPress', () => {
      if (_navigator && _navigator.getCurrentRoutes().length > 1) {
        _navigator.pop();
        return true;
      }
      return false;
    });
  }

  render() {
    return (
      <Navigator
        initialRoute={{ 
          component: Home,
        }}
        renderScene={this.renderScene}
      />
    );
  }
}
