import React, { Component, PropTypes } from 'react'
import { StyleSheet } from 'react-native'
import { NavigationBar, View, ListView, Row, Button, Title, Subtitle, Text, Caption, Icon, Divider } from '@shoutem/ui';
import moment from 'moment';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux'

import ListNotificationItem from '../components/ListNotificationItem'
import ItemDetailView from './ItemDetailView'
import * as authActions from '../actions/menu'

class RightMenu extends Component {
	constructor(props) {
		super(props);

        this.state = {
            items: [{
                title: 'Tip titles 1',
            },
            {
                title: 'Tip title 2',
            },
            ]
        };
	}

	_closeRightDrawer = () => Actions.refresh({key: 'NavigationDrawer', showLeft: false, showRight: false})

    onPressItem = (item) => {
        this._closeRightDrawer();
        const { navigator } = this.props;

        navigator.push({
            component: ItemDetailView,
            passProps: {
                index: 1,
                navigator: navigator,
                data: item
            }
        });
    }

	render() {
		return (
            <View style={styles.container}>
                <View style={styles.barStyle}>
                    <Text style={styles.barText}>Golden tips</Text>
                </View>
                <ListView
                    data={this.state.items}
                    loading={false}
                    renderRow={(item) => <ListNotificationItem data={item} type='View2' style={styles.purchase} onPressItem={this.onPressItem} />}
                />
            </View>
        )
    }
}

styles = {
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    barStyle: {
        marginTop: 40,
        padding: 5,
        backgroundColor: '#d8c80c',
    },
    barText: {
        paddingLeft: 5,
        paddingRight: 5,
        // fontSize: 12,
        // fontWeight: '500',
        color: 'white',
    },
}

RightMenu.propTypes = {
    navigator: PropTypes.object,
}

function select(store) {
	return {
		navigator: store.menu.navigator,
	};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(authActions , dispatch);
}

export default connect(select, mapDispatchToProps)(RightMenu)