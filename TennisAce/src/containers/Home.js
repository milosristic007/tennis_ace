import React, { Component, PropTypes } from 'react';
import {
    Platform,
    ActivityIndicator,
    Dimensions,
    Text,
    View,
    TouchableOpacity,
    AlertIOS,
    AsyncStorage,
    StyleSheet
} from 'react-native';
import NavigationBar from 'react-native-navbar';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';
import { ListView, Icon } from '@shoutem/ui';
import { AdMobBanner } from 'react-native-admob'
import OneSignal from 'react-native-onesignal';

import LeftMenuBTN from '../components/LeftMenuBTN'
import RightMenuBTN from '../components/RightMenuBTN'
import ListItem from '../components/ListItem'
import ItemDetailView from './ItemDetailView'

import * as authActions from '../actions/auth'
import * as menuActions from '../actions/menu'
import * as Configs from '../data/appconfig'


class Home extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            isLoaded: false,
            error: false,
            contents: [null],
        };
        // this.getContents = this.getContents.bind(this);
    }

    componentWillMount() {
        this.props.setNavigator(this.props.navigator);
        this.getContents();
        OneSignal.addEventListener('received', this.onReceived);
        OneSignal.addEventListener('opened', this.onOpened);
    }

    componentWillUnmount() {
        OneSignal.removeEventListener('received', this.onReceived);
        OneSignal.removeEventListener('opened', this.onOpened);
    }

    onReceived(notification) {
        this.getContents();
        console.log("Notification received: ", notification);
        // alert('received notification');
    }

    onOpened(openResult) {
        this.getContents();
        console.log('Message: ', openResult.notification.payload.body);
        console.log('Data: ', openResult.notification.payload.additionalData);
        console.log('isActive: ', openResult.notification.isAppInFocus);
        console.log('openResult: ', openResult);
    }

    async getContents() {
        try {
            console.log('1. start getting contents from Storage');
            this.setState({ isLoading: true, isLoaded: false });
            let value = await AsyncStorage.getItem(Configs.STORAGE_CONTENTS);
            if (value !== null) {
                let contents = JSON.parse(value);
                this.setState({ contents: contents.tips });
                this.setState({ isLoading: false, isLoaded: true });
            }
            this.fetchContents();
        } catch (e) {
            // this.setState({ isLoading: false, error: true });
            console.log('1 error:', e);
        }
    }

    fetchContents = async () => {
        try {
            console.log('2. getting contents from server');
            let response = await fetch(Configs.API_URL_CONTENTS, {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this.props.authTokenLogin.token
                },
            });
            if (response !== null) {
                let jsonData = await response.json();
                await AsyncStorage.setItem(Configs.STORAGE_CONTENTS, JSON.stringify(jsonData));
                this.setState({ isLoading: false, isLoaded: true, contents: jsonData.tips });
                console.log('contents: ', jsonData);
            }
        } catch (e) {
            // this.setState({ isLoading: false, error: true });
            console.log('2 -- error:', e);
        }
    }

    onPressItem = (content) => {
        const { navigator } = this.props;
        navigator.push({
            component: ItemDetailView,
            passProps: {
                index: 1,
                navigator: navigator,
                data: content
            }
        });
    }

	_openLeftDrawer = () => Actions.refresh({key: 'NavigationDrawer', showLeft: true, showRight: false})
	_openRightDrawer = () => Actions.refresh({key: 'NavigationDrawer', showLeft: false, showRight: true})

    render() {

        return (
            (!this.state.isLoaded) ?
                <View style={styles.container} >
                    <ActivityIndicator
                        animating={true}
                        size="large"
                        style={styles.indicator}
                    />
                </View>
                :
                <View style={styles.container} >
                    <NavigationBar
                        leftButton={<LeftMenuBTN style={styles.leftButtonContainer} onPress={() => { this._openLeftDrawer() }} />}
                        rightButton={<RightMenuBTN style={styles.rightButtonContainer} onPress={() => { this._openRightDrawer() }} />}
                    />
                    <View style={styles.contentView}>
                        {(this.state.error == true || this.state.contents[0] === null) ?
                            null
                            :
                            <ListView
                                data={this.state.contents}
                                loading={false}
                                onRefresh={() => { this.getContents() }}
                                renderRow={(content) => <ListItem data={content} type='View1' onPressItem={this.onPressItem} />}
                            />
                        }
                    </View>
                    <AdMobBanner
                        style={styles.bannerView}
                        adUnitID={Platform.OS == 'ios' ? Configs.ADMOB_ID_IOS : Configs.ADMOB_ID_ANDROID}
                        testDeviceID={'EMULATOR'}
                        adViewDidReceiveAd ={null}
                        didFailToReceiveAdWithError={(err) => {
							  //no internet
							  console.log(err);}} />
                </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    indicator: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    contentView: {
        flex: 1,
        // paddingTop: (Platform.OS === 'ios'? 64 : 54),
    },
    leftButtonContainer: {
        width: 40,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
    },
    rightButtonContainer: {
        width: 40,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
    },
    bannerView: {
        width: Dimensions.get('window').width,
        justifyContent: 'flex-end',
        alignItems: 'center',
        backgroundColor: '#f7f7f7'
    },
});

Home.propTypes = {
    isLoading: PropTypes.bool,
    contents: PropTypes.array,
};

function select(store) {
    return {
        isLoggedIn: store.user.isLoggedIn,
        authTokenLogin: store.user.authTokenLogin,
        user: store.user
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators((authActions, menuActions), dispatch);
}

export default connect(select, mapDispatchToProps)(Home)

