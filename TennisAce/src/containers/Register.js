import React, { Component, PropTypes } from 'react';
import {
  ActivityIndicator,
  Dimensions,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  AsyncStorage
} from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';

import * as Configs from '../data/appconfig';
import * as authActions from '../actions/auth';


async function timeout(ms) {
  return new Promise((resolve, reject) => {
    setTimeout(() => reject(new Error('Timed out')), ms);
  });
}

class Register extends Component {
	constructor(props) {
		super(props);
		this.state = {
			authTokenAdmin: null,
      authTokenLogin: null,
			username: '',//zoltan_teszari',
			password: '',//'123456789',
			email: '',//'asterism@yahoo.com',
			firstname: '',//'Zoltan',
			lastname: '',//'Teszari',
      isLoading: false,
		};
    this.handleSingUp = this.handleSingUp.bind(this);
    this.onRegister = this.onRegister.bind(this);
    this.onSignIn = this.onSignIn.bind(this);
	}

  componentWillMount() {
		this.getPersistedToken();
  }

	async getPersistedToken(){
		try{
			let loginToken = await AsyncStorage.getItem(Configs.STORAGE_LOGIN_TOKEN);
			if (loginToken !== null ) {
				console.log('login token from storage', loginToken);
				this.setState({authTokenLogin: JSON.parse(loginToken)});
        let retValue = await this.isValidateToken(this.state.authTokenLogin);
        if( retValue ) {
          this.props.setLogIn(this.state);
          Actions.NavigationDrawer();
          return; 
        }
			}
			let value = await AsyncStorage.getItem(Configs.STORAGE_KEY);
			if (value === null ) {
				this.setAdminToken();
			} else {
				console.log('Value from storage', value);
				this.setState({authTokenAdmin: JSON.parse(value)});
        let retValue = await this.isValidateToken(this.state.authTokenAdmin);
        if( !retValue ) { 
          this.setAdminToken();
        }
			}
		} catch (error) {
			console.log('AsyncStorage error', error);
		}
	}

	async isValidateToken(authToken){
		try{
        let response = await fetch(Configs.API_URL_LOGIN_TOKEN, {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + authToken.token,
          },
        });
        console.log('token response:', JSON.stringify(response));
        if (response.ok) {
          return true;
        }
        return false;
    } catch (error) {
			console.log('AsyncStorage error', error);
		}
	}

	async setAdminToken(){
		const wpAppUserUsername = 'nativeapp';
		const wpAppUserPassword = 'cT%4DPSRlbT#mHbR)Pm5xo@L';
		try{
			let responseJson = await this.getAuthToken(wpAppUserUsername, wpAppUserPassword);
			console.log('Response from server', responseJson);
			this.setState({authTokenAdmin: responseJson});
      if (responseJson !== null) {
        AsyncStorage.setItem(Configs.STORAGE_KEY, JSON.stringify(responseJson));
        this.props.setAuthToken(this.state);
      }
		} catch (error) {
			console.log('Set admin token error', error);
		}
	}

	async getAuthToken(username, password){
		try{
			let data = {
				username: username,
				password: password
			}
			let json = JSON.stringify(data);
			let response = await fetch(Configs.API_URL_LOGIN, {
				method: 'POST',
				headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json',
				},
				body: json
			});
			if (response.ok) {
				let responseJson = await response.json();
				if (responseJson.token){
					return responseJson;
				}
			} else {
				throw new Error('Network response was not ok.');
			}
		} catch (error) {
			console.log('Fetch error', error);
		}
	}

	async handleSingUp(){
    if( this.state.username === '' || this.state.email ==='' || this.state.password ==='' ||
        this.state.firstname === '' || this.state.lastname ==='' ) {
      alert('please put the correct informations!')
    }
    else {
      try {
        if (this.state.authTokenAdmin === null) {
          await this.setAdminToken();
        }
        let response = await fetch(Configs.API_URL_SINUP, {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.state.authTokenAdmin.token
          },
          body: JSON.stringify({
            username: this.state.username,
            email: this.state.email,
            password: this.state.password,
            first_name: this.state.firstname,
            last_name: this.state.lastname
          })
        });
        if (response.ok) {
          let responseJson = await response.json();
          try {
            let userJson = await this.getAuthToken(responseJson.username, this.state.password);
            if (userJson !== null && userJson.token) {
              AsyncStorage.setItem(Configs.STORAGE_LOGIN_TOKEN, JSON.stringify(userJson));
              this.setState({ authTokenLogin: userJson });
              this.props.setSignUp(this.state);
              Actions.NavigationDrawer();
              // this._navigate(TipPage);
            } else {
              // this._navigate()
            }
          } catch (tokenError) {
            console.log('Get token error ', tokenError);
            this.setState({ isLoading: false });
          }
        }
        console.log('Sign up parameters: ', JSON.stringify({
            username: this.state.username,
            email: this.state.email,
            password: this.state.password,
            first_name: this.state.firstname,
            last_name: this.state.lastname
          })+'  token:'+this.state.authTokenAdmin.token);
        console.log('Sign up response: ', JSON.stringify(response));
        this.setState({ isLoading: false });
      } catch (error) {
        console.log('Create user error', error);
        this.setState({ isLoading: false });
      }
    }
	}

	_navigate(component){
		this.props.navigator.push({
			component: component, 
			authTokenAdmin: this.state.authTokenAdmin
		})
	}

  async onRegister() {
    this.setState({ isLoading: true });
    try {
      await Promise.race([
        this.handleSingUp(),
        timeout(15000),
      ]);
      this.setState({ isLoading: false });
    } catch (e) {
      const message = e.message || e;
      if (message !== 'Timed out' && message !== 'Canceled by user') {
        alert(message);
      }
      this.setState({ isLoading: false });
      return;
    } finally {
      if (this._isMounted) {
        this.setState({ isLoading: false });
      }
    }
  }

  async onSignIn() {
    this.props.setAuthToken(this.state);
    Actions.Login();
  }

  render() {
    return (
      <View style={styles.container}>
          {this.state.isLoading || this.props.isLoading
            ?
            <ActivityIndicator
              animating='true'
              size="large"
              style={styles.indicator}
            />
            :
            <View style={styles.container}>
              <View style={styles.wrapper}>
                <View style={styles.inputWrap}>
                  <TextInput 
                    placeholder="Username" 
                    placeholderTextColor="#000"
                    underlineColorAndroid='transparent'
                    style={styles.input} 
                    onChange={ (event) => this.setState({username: event.nativeEvent.text}) } 
                    value={ this.state.username }
                  />
                </View>
                <View style={styles.inputWrap}>
                  <TextInput 
                    placeholder="Email" 
                    placeholderTextColor="#000"
                    underlineColorAndroid='transparent'
                    style={styles.input} 
                    onChange={ (event) => this.setState({email: event.nativeEvent.text}) } 
                    value={ this.state.email }
                  />
                </View>
                <View style={styles.inputWrap}>
                  <TextInput 
                    placeholder="Password" 
                    placeholderTextColor="#000"
                    underlineColorAndroid='transparent'
                    style={styles.input} 
                    onChange={ (event) => this.setState({password: event.nativeEvent.text}) } 
                    value={ this.state.password }
                    secureTextEntry 
                  />
                </View>
                <View style={styles.inputWrap}>
                  <TextInput 
                    placeholder="First name" 
                    placeholderTextColor="#000"
                    underlineColorAndroid='transparent'
                    style={styles.input} 
                    onChange={ (event) => this.setState({firstname: event.nativeEvent.text}) } 
                    value={ this.state.firstname }
                  />
                </View>
                <View style={styles.inputWrap}>
                  <TextInput 
                    placeholder="Last name" 
                    placeholderTextColor="#000"
                    underlineColorAndroid='transparent'
                    style={styles.input} 
                    onChange={ (event) => this.setState({lastname: event.nativeEvent.text}) } 
                    value={ this.state.lastname }
                  />
                </View>
                <TouchableOpacity style={styles.button} onPress={this.handleSingUp} activeOpacity={.5}>
                  <Text style={styles.buttonText}>Register now</Text>
                </TouchableOpacity>
              </View>
              <View style={styles.container}>
                <View style={styles.signupWrap}>
                  <Text style={styles.accountText}>
                    Already have a BetSet account?
                  </Text>
                </View>
                <TouchableOpacity  onPress={this.onSignIn} activeOpacity={.5}>
                  <View>
                    <Text style={styles.signupLinkText}>Sign in now</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  indicator: {
    flex: 1,
    color: 'gray',
    alignItems: 'center',
    justifyContent: 'center'
  },
  wrapper: {
    paddingHorizontal: (Dimensions.get('window').width<641 ? 30:(Dimensions.get('window').width-500)/2),
    backgroundColor: 'transparent', 
    marginTop: (Dimensions.get('window').height<800 ? 80 : 200), 
    alignItems: 'center'
  },
  inputWrap: {
    flexDirection: "row",
    marginVertical: 10,
    height: 40,
    borderBottomWidth: 1,
    borderBottomColor: "#CCC"
  },
  input: {
    flex: 1,
    paddingHorizontal: 10
  },
  button: {
    flexDirection: 'row',
    marginVertical: 10,
    marginTop: 30,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#387afe",
  },
  buttonText: {
    flex: 1,
    color: "#FFF",
    fontSize: 18,
    fontWeight: '500',
    textAlign: 'center'
  },
  signupWrap: {
    marginTop: 35,
    backgroundColor: 'transparent',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  accountText: {
    color: 'gray'
  },
  signupLinkText: {
    color: '#387afe',
    fontSize: 18,
    fontWeight: '500',
    textAlign: 'center'
  }
});

Register.propTypes = {
  isLoading: PropTypes.bool,
};

function select(store) {
  return {
    authTokenAdmin: store.user.authTokenAdmin,
    authTokenLogin: store.user.authTokenLogin,
    isLoading: store.user.isLoading
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(authActions , dispatch);
}

export default connect(select, mapDispatchToProps)(Register);
