import { Answers } from 'react-native-fabric';

const initialState = {
  openModals: [
    {
      key: 'InitialRoute',
      type: 'Home'
    }
  ],
};

function navigation(state = initialState, action) {
  switch (action.type) {
    case 'BACK':
      if (state.openModals.length > 1) {
        return {
          ...state,
          openModals: state.openModals.slice(0, state.openModals.length - 1),
        };
      }
      return state;

    case 'LOGGED_IN':
      const nonLoginModals = state.openModals.filter(modal => modal.type !== 'InitialLogin');
      if (state.openModals.length !== nonLoginModals.length) {
        return {
          ...state,
          openModals: nonLoginModals,
        };
      }
      return state;

    // case 'SIGNUP_IN':
    //   const nonLoginModals = state.openModals.filter(modal => modal.type !== 'InitialLogin');
    //   if (state.openModals.length !== nonLoginModals.length) {
    //     return {
    //       ...state,
    //       openModals: nonLoginModals,
    //     };
    //   }
    //   return state;

    // case 'OPEN_PROFILE_SETTINGS':
    //   return {
    //     ...state,
    //     openModals: [
    //       ...state.openModals,
    //       {
    //         key: `VerticalModal-${Date.now()}`,
    //         type: 'ProfileSettings'
    //       }
    //     ],
    //   };

    case 'LOGGED_OUT':
      return initialState;

    default:
      return state;
  }
}

export default navigation;
