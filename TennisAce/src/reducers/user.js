
import * as types from '../actions/actionTypes'

const initialState = {
  id: null,
  authTokenAdmin: null,
  authTokenLogin: null,
  username: '',
  password: '',
  email: '',
  firstname: '',
  lastname: '',
  isLoggedIn: false,
  isLoging: false,
  isSignUp: false,
  isLoading: false,
};

function user(state = initialState, action) {
  switch (action.type) {
    case types.USER_AUTH_TOKEN:
      return {
        ...state,
        authTokenAdmin: action.authTokenAdmin,
      };

    case types.USER_LOG_IN:
      return {
        ...state,
        isLoging: true,
        isSignUp: false,
      };

    case types.USER_SIGN_UP:
      return {
        ...state,
        isLoging: false,
        isSignUp: true,
      };

    case types.USER_SIGNED_UP:
      return {
        ...state,
        authTokenAdmin: action.authTokenAdmin,
        authTokenLogin: action.authTokenLogin,
        id: action.id,
        username: action.username,
        email: action.email,
        pass: action.pass,
        firstname: action.firstname,
        lastname: action.lastname,
        isLoggedIn: true,
        isLoging: false,
        isSignUp: false,
        isLoading: false,
      };

    case types.USER_LOGGED_IN:
      return {
        ...state,
        authTokenLogin: action.authTokenLogin,
        id: action.id,
        username: action.username,
        email: action.email,
        pass: action.pass,
        isLoggedIn: true,
        isLoging: false,
        isSignUp: false,
        isLoading: false,
      };

    case types.USER_LOG_OUT:
      return initialState;

    default:
      return state;
  }
}

export default user;
