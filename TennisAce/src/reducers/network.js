const initialState = {
  isConnected: true
};

function network(state = initialState, action) {
  switch (action.type) {
    case 'SET_IS_CONNECTED':
      return {
        ...state,
        isConnected: action.isConnected
      };

    default:
      return state;
  }
}

export default network;
