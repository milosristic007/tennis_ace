import * as types from '../actions/actionTypes'

const initialState = {
  showLeft: false,
  showRight: false,
  navigator: null,
};

function menu(state = initialState, action) {
  switch (action.type) {
    case types.MENU_LEFT_STATE:
      return {
        ...state,
        showLeft: action.showLeft,
      };

    case types.MENU_RIGHT_STATE:
      return {
        ...state,
        showRigth: action.showRight,
      };

    case types.MENU_NAVIGATOR:
      return {
        ...state,
        navigator: action.navigator,
      };

    default:
      return state;
  }
}

export default menu;
