import { combineReducers } from 'redux';
import user from './user';
import menu from './menu';
import network from './network';



export default combineReducers({
  user,
  menu,
  network,
});
